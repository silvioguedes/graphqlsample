package com.silvioapps.graphqlsample;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class Utils {
    public static int getYear(Date date) {
        Calendar calendar = Calendar.getInstance();
        int year = -1;

        if (date != null) {
            calendar.setTime(date);
            try {
                year = calendar.get(Calendar.YEAR);
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }

        return year;
    }

    public static int getMonth(Date date) {
        Calendar calendar = Calendar.getInstance();
        int month = -1;

        if (date != null) {
            calendar.setTime(date);
            try {
                month = calendar.get(Calendar.MONTH);
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }

        return month + 1;
    }

    public static int getDay(Date date) {
        Calendar calendar = Calendar.getInstance();
        int day = -1;

        if (date != null) {
            calendar.setTime(date);
            try {
                day = calendar.get(Calendar.DAY_OF_MONTH);
            } catch (ArrayIndexOutOfBoundsException e) {
                e.printStackTrace();
            }
        }

        return day;
    }
}
