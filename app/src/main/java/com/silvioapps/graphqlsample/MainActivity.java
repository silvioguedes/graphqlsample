package com.silvioapps.graphqlsample;

import androidx.appcompat.app.AppCompatActivity;
import io.reactivex.Observer;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import com.apollographql.apollo.ApolloCall;
import com.apollographql.apollo.ApolloClient;
import com.apollographql.apollo.api.Operation;
import com.apollographql.apollo.api.ResponseField;
import com.apollographql.apollo.api.cache.http.HttpCachePolicy;
import com.apollographql.apollo.cache.http.ApolloHttpCache;
import com.apollographql.apollo.cache.http.DiskLruHttpCacheStore;
import com.apollographql.apollo.cache.normalized.CacheKey;
import com.apollographql.apollo.cache.normalized.CacheKeyResolver;
import com.apollographql.apollo.cache.normalized.NormalizedCacheFactory;
import com.apollographql.apollo.cache.normalized.lru.EvictionPolicy;
import com.apollographql.apollo.cache.normalized.lru.LruNormalizedCacheFactory;
import com.apollographql.apollo.cache.normalized.sql.ApolloSqlHelper;
import com.apollographql.apollo.cache.normalized.sql.SqlNormalizedCacheFactory;
import com.apollographql.apollo.exception.ApolloException;
import com.apollographql.apollo.response.CustomTypeAdapter;
import com.apollographql.apollo.response.CustomTypeValue;
import com.apollographql.apollo.rx2.Rx2Apollo;
import com.silvioapps.graphqlsample.type.CustomType;
import org.jetbrains.annotations.NotNull;
import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import javax.annotation.Nonnull;

public class MainActivity extends AppCompatActivity {
    public static final String BASE_URL = "https://api.graph.cool/simple/v1/ciyz901en4j590185wkmexyex";
    public static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy-MM-dd", Locale.ENGLISH);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button showAllUsersButton = findViewById(R.id.showAllUsersButton);
        showAllUsersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                queryAllUsersRxJava();
            }
        });

        Button findUserButton = findViewById(R.id.findUserButton);
        findUserButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EditText emailEditText = findViewById(R.id.emailEditText);
                queryUserRxJava(emailEditText.getText().toString());
            }
        });
    }

    protected ApolloClient setupApollo(){
        OkHttpClient okHttp = new OkHttpClient
                .Builder()
                .addInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request original = chain.request();
                        Request.Builder builder = original.newBuilder().method(original.method(),
                                original.body());
                        return chain.proceed(builder.build());
                    }
                })
                .build();

        //Create the ApolloSqlHelper. Please note that if null is passed in as the name, you will get an in-memory SqlLite database that
        // will not persist across restarts of the app.
        ApolloSqlHelper apolloSqlHelper = ApolloSqlHelper.create(this, "db_name");

        //Create NormalizedCacheFactory
        NormalizedCacheFactory sqlNormalizedCacheFactory = new SqlNormalizedCacheFactory(apolloSqlHelper);

        //Normalized In-Memory Cache
        NormalizedCacheFactory normalizedCacheFactory = new LruNormalizedCacheFactory(EvictionPolicy.builder()
                .maxSizeBytes(10 * 1024).build())
                /* Chaining Caches */
                .chain(sqlNormalizedCacheFactory);

        return ApolloClient
                .builder()
                .serverUrl(BASE_URL)
                .okHttpClient(okHttp)
                .httpCache(new ApolloHttpCache(getCacheSettings()))
                .addCustomTypeAdapter(CustomType.DATETIME, getCustomTypeAdapter())
                .normalizedCache(normalizedCacheFactory, getCacheKeyResolver())
                .build();
    }

    protected DiskLruHttpCacheStore getCacheSettings(){
        String fileName = "someFileName";
        File file = new File(getExternalFilesDir(null), fileName);

        //Size in bytes of the cache
        int size = 1024*1024;

        //Create the http response cache store
        return new DiskLruHttpCacheStore(file, size);
    }

    protected CustomTypeAdapter getCustomTypeAdapter(){
        return new CustomTypeAdapter<Date>() {
            @Override
            public Date decode(@NotNull CustomTypeValue value) {
                try {
                    DATE_FORMAT.setLenient(true);
                    return DATE_FORMAT.parse(value.value.toString());
                } catch (ParseException e) {
                    throw new RuntimeException(e);
                }
            }

            @Override
            public CustomTypeValue encode(@NotNull Date value) {
                return new CustomTypeValue.GraphQLString(DATE_FORMAT.format(value));
            }
        };
    }

    protected CacheKeyResolver getCacheKeyResolver(){
        //Create the cache key resolver, this example works well when all types have globally unique ids.
        return new CacheKeyResolver() {
            @NotNull @Override
            public CacheKey fromFieldRecordSet(@NotNull ResponseField field, @NotNull Map<String, Object> recordSet) {
                return formatCacheKey((String) recordSet.get("id"));
            }

            @NotNull @Override
            public CacheKey fromFieldArguments(@NotNull ResponseField field, @NotNull Operation.Variables variables) {
                return formatCacheKey((String) field.resolveArgument("id", variables));
            }

            private CacheKey formatCacheKey(String id) {
                if (id == null || id.isEmpty()) {
                    return CacheKey.NO_KEY;
                } else {
                    return CacheKey.from(id);
                }
            }
        };
    }

    protected void queryAllUsers(){
        final TextView resultAllUsersTextView = findViewById(R.id.resultAllUsersTextView);
        resultAllUsersTextView.setText("");

        setupApollo()
                .query(UsersQuery
                .builder()
                .build())
                .httpCachePolicy(HttpCachePolicy.NETWORK_ONLY)
                .enqueue(new ApolloCall.Callback<UsersQuery.Data>() {
                    @Override
                    public void onResponse(final @Nonnull com.apollographql.apollo.api.Response<UsersQuery.Data> response) {
                        if(response.data() != null && response.data().allUsers() != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    for(UsersQuery.AllUser user : response.data().allUsers()){
                                        resultAllUsersTextView.append("user.name "+user.name()+"\n");
                                        resultAllUsersTextView.append("user.email "+user.email()+"\n");
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(final @Nonnull ApolloException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                resultAllUsersTextView.setText("error "+e.getMessage());
                            }
                        });
                    }
                });
    }

    protected void queryAllUsersRxJava(){
        final TextView resultAllUsersTextView = findViewById(R.id.resultAllUsersTextView);
        resultAllUsersTextView.setText("");

        Rx2Apollo
                .from(setupApollo().query(UsersQuery.builder().build()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<com.apollographql.apollo.api.Response<UsersQuery.Data>>() {
                    @Override
                    public void onSubscribe(Disposable d) {}

                    @Override
                    public void onNext(final com.apollographql.apollo.api.Response<UsersQuery.Data> dataResponse) {
                        if(dataResponse.data() != null && dataResponse.data().allUsers() != null) {
                            for(UsersQuery.AllUser user : dataResponse.data().allUsers()){
                                resultAllUsersTextView.append("user.name "+user.name()+"\n");
                                resultAllUsersTextView.append("user.email "+user.email()+"\n");
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        resultAllUsersTextView.setText("error "+e.getMessage());
                    }

                    @Override
                    public void onComplete() {}
                });
    }

    protected void queryUser(String email){
        final TextView resultFindUserTextView = findViewById(R.id.resultFindUserTextView);
        resultFindUserTextView.setText("");

        setupApollo()
                .query(UsersQuery
                .builder()
                .email(email)
                .build())
                .httpCachePolicy(HttpCachePolicy.CACHE_FIRST.expireAfter(10, TimeUnit.SECONDS))
                .enqueue(new ApolloCall.Callback<UsersQuery.Data>() {
                    @Override
                    public void onResponse(final @Nonnull com.apollographql.apollo.api.Response<UsersQuery.Data> response) {
                        if(response.data() != null && response.data().User() != null) {
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    resultFindUserTextView.append("user.name " + response.data().User().name()+"\n");
                                    for (UsersQuery.Post post : response.data().User().posts()) {
                                        resultFindUserTextView.append("post.text " + post.text()+"\n");

                                        Date date = (Date)post.createdAt();
                                        resultFindUserTextView.append("post.createdAt " + Utils.getDay(date) + "/" + Utils.getMonth(date) + "/" + Utils.getYear(date)+"\n");
                                    }
                                }
                            });
                        }
                    }

                    @Override
                    public void onFailure(final @Nonnull ApolloException e) {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                resultFindUserTextView.setText("error " + e.getMessage());
                            }
                        });
                    }
                });
    }

    @SuppressLint("CheckResult")
    protected void queryUserRxJava(String email){
        final TextView resultFindUserTextView = findViewById(R.id.resultFindUserTextView);
        resultFindUserTextView.setText("");

        Rx2Apollo.from(setupApollo().query(UsersQuery.builder().email(email).build()))
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(new Observer<com.apollographql.apollo.api.Response<UsersQuery.Data>>() {
                    @Override
                    public void onSubscribe(Disposable d) {}

                    @Override
                    public void onNext(final com.apollographql.apollo.api.Response<UsersQuery.Data> dataResponse) {
                        if(dataResponse.data() != null && dataResponse.data().User() != null) {
                            resultFindUserTextView.append("user.name " + dataResponse.data().User().name() + "\n");
                            for (UsersQuery.Post post : dataResponse.data().User().posts()) {
                                resultFindUserTextView.append("post.text " + post.text() + "\n");

                                Date date = (Date) post.createdAt();
                                resultFindUserTextView.append("post.createdAt " + Utils.getDay(date) + "/" + Utils.getMonth(date) + "/" + Utils.getYear(date) + "\n");
                            }
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        resultFindUserTextView.setText("error " + e.getMessage());
                    }

                    @Override
                    public void onComplete() {}
                });
    }
}
